%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : channel.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 7 Jul. 2021
% Description 	: digital communications channel class
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                           CHANNEL CLASS 
%---------------------------------------------------------------------%

classdef channel

%---------------------------------------------------------------------%
%                           PRIVATE PROPIETIES
%---------------------------------------------------------------------%

    properties 
        noiseVariance {mustBeReal} = 0 ; % Varianza de ruido COMPLEJO por Hz (N0)
    end

%---------------------------------------------------------------------%
%                           PUBLIC METHODS
%---------------------------------------------------------------------%

    methods

        %-------------------------------------------------------------%
        %                         CONSTRUCTOR
        %-------------------------------------------------------------%
        function obj = channel(configStruc)

            if nargin == 0
                return
            end

            fields = fieldnames(configStruc);
            for i = 1:length(fields)
                switch fields{i}  
                    case 'noiseVariance'
                        obj.noiseVariance = configStruc.noiseVariance;

                    otherwise
                        error(strcat('Invalid configuration field -> ',fields{i}));
                end
            end
        end

        %-------------------------------------------------------------%
        %                         GETTER FUNCTIONS
        %-------------------------------------------------------------%

        %-------------------------------------------------------------%
        %                         SETTER FUNCTIONS
        %-------------------------------------------------------------%

        function obj = set.noiseVariance(obj, variance)
            obj.noiseVariance = variance;
        end

        %-------------------------------------------------------------%
        %                         MAIN FUNCTION
        %-------------------------------------------------------------%

        function ch_out = addNoise(obj,tx_out)
            % addNoise - Add complex noise to tx_out
            %
            % Syntax: ch_out = addNoise(tx_out)
            
            if isreal(tx_out)
                channelDesviation = sqrt(obj.noiseVariance/2);
                noise = channelDesviation * randn(size(tx_out));
            else
                channelDesviation = sqrt(obj.noiseVariance/2);
                realNoise = channelDesviation * randn(size(tx_out)) ;
                imagNoise = channelDesviation * randn(size(tx_out)) ;
                noise = realNoise + 1j * imagNoise ;    
            end

            ch_out = tx_out + noise;
        end

    end

%---------------------------------------------------------------------%
%                           PRIVATE METHODS
%---------------------------------------------------------------------%


%---------------------------------------------------------------------%
%                       RECEIVER CLASS END
%---------------------------------------------------------------------%
end