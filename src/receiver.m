%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : receiver.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 7 Jul. 2021
% Description 	: digital communications receiver class
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                           RECEIVER CLASS 
%---------------------------------------------------------------------%

classdef receiver

%---------------------------------------------------------------------%
%                           PRIVATE PROPIETIES
%---------------------------------------------------------------------%
    properties (GetAccess = public, SetAccess = private)
        % All Rx 
        symbolRate           {mustBePositive, mustBeReal    } = 1e9 ; % Baud Rate BR
        
        % Slicer
        modulationType       {mustBeInteger                 } = 1   ; % Tipo de transmisor (0:PAM o 1:QAM)
        mapperLevels         {mustBePositive, mustBeInteger } = 2   ; % Cantidad de niveles del mapper M
        
        % Matchet Filter 
        filterType           {mustBeInteger                 } = 1   ; % Tipo de filtro conformador (0:RC o 1:RRC)
        filterLength         {mustBePositive, mustBeInteger } = 500 ; % Coeficientes del filtro
        filterRolloff        {mustBePositive, mustBeReal    } = 0.1 ; % Exceso de ancho de banda del filtro
        
        oversamplingFactor   {mustBePositive, mustBeInteger } = 8   ; % Factor de sobremuestreo
        
        % Correlator
        correlatorPhase      {mustBeInteger                 } = 0   ; % Fase de muestreo del correlador 
        
        % Down-Converter
        dwConvPhaseError     {mustBeReal                    } = 0   ; % Error de fase [deg] del down-converter
        dwConvfrequencyOffset{mustBeReal                    } = 0   ; % Offset de frecuencia [Hz] del down converter
    
    end

%---------------------------------------------------------------------%
%                         DEPENDENT PROPIETIES
%---------------------------------------------------------------------%
    properties (Dependent)
        % Matched filter  
        filterTaps                                                   % Cantidad de coeficientes del filtro
        filterSamplingFreq                                           % Frecuencia  de sampling  del filtro
        filterPower                                                  % Potencia del del filtro
    end
    
%---------------------------------------------------------------------%
%                           PUBLIC METHODS
%---------------------------------------------------------------------%
    methods

        function obj = receiver(configStruc)
            % receiver - Return an receiver object
            %
            % Syntax: rx = receiver()

            if nargin == 0
                return
            end

            fields = fieldnames(configStruc);
            % Define campos contenidos en 'fields'
            for i = 1:length(fields)
                switch fields{i}
                    case 'modulationType'
                        if configStruc.modulationType ~= 0 && configStruc.modulationType ~= 1
                            error('Modulation selected type is invalid');
                        else
                            obj.modulationType    = configStruc.modulationType        ;
                        end
                    case 'symbolRate'
                        obj.symbolRate            = configStruc.symbolRate            ;
                    case 'mapperLevels'
                        obj.mapperLevels          = configStruc.mapperLevels          ;
                    case 'filterType'
                        if configStruc.filterType ~= 0 && configStruc.filterType ~= 1
                            error('Filter type selected is invalid');
                        else
                            obj.filterType        = configStruc.filterType            ;
                        end
                    case 'filterLength'
                        obj.filterLength          = configStruc.filterLength          ;
                        obj.filterLength          = obj.filterLength - ...
                                                    mod(obj.filterLength,2) + 1       ; % Force odd
                    case 'filterRolloff'
                        obj.filterRolloff         = configStruc.filterRolloff         ;
                    case 'oversamplingFactor'
                        obj.oversamplingFactor    = configStruc.oversamplingFactor    ;
                    case 'correlatorPhase'
                        obj.correlatorPhase       = configStruc.correlatorPhase       ;
                    case 'dwConvPhaseError'
                        obj.dwConvPhaseError      = configStruc.dwConvPhaseError      ;
                    case 'dwConvfrequencyOffset'
                        obj.dwConvfrequencyOffset = configStruc.dwConvfrequencyOffset ;
                    otherwise
                        error(strcat('Invalid configuration field -> ',fields{i}));
                end
            end
        end

        function fs = get.filterSamplingFreq(obj)
            % get.filterSamplingFreq - Calculate the filter sample rate
            %
            % Syntax: fs = get.filterSamplingFreq(obj)
            fs = obj.oversamplingFactor * obj.symbolRate ;
        end
        
        function filtertaps = get.filterTaps(obj)
            % get.filterTaps - Calculate pulse shaping filter coefficients
            %
            % Syntax: taps = get.filterTaps(obj)
            if obj.filterType == 1
                filtertaps = raiz_cos_realzado(obj.symbolRate, obj.filterSamplingFreq, obj.filterRolloff, obj.filterLength);
            else
                filtertaps = cos_realzado(obj.symbolRate, obj.filterSamplingFreq, obj.filterRolloff, obj.filterLength);
            end
        end

        function Eh = get.filterPower(obj)
            % get.filterPower - Calculate the filter power 
            %
            % Syntax: fs = get.filterPower(obj)
            Eh = sum(abs(obj.filterTaps).^2);
        end
        
        function outstruct = getConfigStruct(obj)
            % getConfigStruct - Return the transmitter configuration struct 
            %
            % Syntax: configStruc = getConfigStruct()
            
            outstruct.modulationType        = obj.modulationType         ;
            outstruct.symbolRate            = obj.symbolRate             ;
            outstruct.mapperLevels          = obj.mapperLevels           ;
            outstruct.filterType            = obj.filterType             ;
            outstruct.filterLength          = obj.filterLength           ;
            outstruct.filterRolloff         = obj.filterRolloff          ;
            outstruct.oversamplingFactor    = obj.oversamplingFactor     ;
            outstruct.correlatorPhase       = obj.correlatorPhase        ;
            outstruct.dwConvPhaseError      = obj.dwConvPhaseError       ;
            outstruct.dwConvfrequencyOffset = obj.dwConvfrequencyOffset  ;
        end
    
        function [rx_ak,correlatorOut,matchFilterOut] = receive (obj, ch_out)
            % receive - Return 
            %
            % Syntax: [rx_ak, correlatorOut] = receive()
            
            % Down-Converter
            t = (0:length(ch_out)-1).' .* obj.symbolRate * obj.oversamplingFactor ;
            ch_out = ch_out .* exp(1j*pi*obj.dwConvPhaseError/180 + 1j*2*pi * obj.dwConvfrequencyOffset * t) ;

            % Matchet Filter and Correlator
            matchFilterOut = filter(obj.filterTaps, 1, ch_out);
            y_n = matchFilterOut / obj.filterPower;
            correlatorOut = downsample(y_n,obj.oversamplingFactor,obj.correlatorPhase);
            
            % Slicer
            z_size = size(correlatorOut);
            if obj.modulationType == 0
                rx_ak = -1 * ones(z_size);
                rx_ak(correlatorOut > 0) = 1 ;
            else
                z_real = real(correlatorOut);
                z_imag = imag(correlatorOut);
            
                ak_I = -1 * ones(z_size);
                ak_Q = -1 * ones(z_size);
                
                switch obj.mapperLevels
                    case 2
                        ak_I(z_real > 0) = 1 ;
                        ak_Q(z_imag > 0) = 1 ;
                        
                    case 4
                        ak_I(z_real >= 2 ) =  3;
                        ak_I(z_real < -2 ) = -3;
                        ak_I(z_real >  0 & z_real < 2) = 1;
                        
                        ak_Q(z_imag >= 2 ) =  3;
                        ak_Q(z_imag < -2 ) = -3;
                        ak_Q(z_imag >  0 & z_imag < 2) = 1;                        
    
                    case 8
                        ak_I(z_real >= 4 ) =  5;
                        ak_I(z_real < -4 ) = -5;
                        ak_I(z_real >= 2 & z_real <  4) =  3;
                        ak_I(z_real < -2 & z_real > -4) = -3;
                        ak_I(z_real >  0 & z_real <  2) =  1;
                        
                        ak_Q(z_imag >= 4 ) =  5;
                        ak_Q(z_imag < -4 ) = -5;
                        ak_Q(z_imag >= 2 & z_real <  4) =  3;
                        ak_Q(z_imag < -2 & z_real > -4) = -3;
                        ak_Q(z_imag >  0 & z_real <  2) =  1;                    
                    
                    otherwise
                        error("The selected modulationType is not supported");
                end
                rx_ak = ak_I + 1j * ak_Q;
            end
        end
    end

%---------------------------------------------------------------------%
%                           PRIVATE METHODS
%---------------------------------------------------------------------%


%---------------------------------------------------------------------%
%                       RECEIVER CLASS END
%---------------------------------------------------------------------%

end

