%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : transmitter_test.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 7 Jul. 2021
% Description 	: digital communications transmitter class test
%---------------------------------------------------------------------%


close all

tx_config.modulationType     = 1    ; % Tipo de transmisor = QAM (1)
tx_config.simbolRate         = 32e9 ; % Baud Rate BR
tx_config.mapperLevels       = 2    ; % Cantidad de niveles del mapper M
tx_config.filterType         = 1    ; % Tipo de filtro conformador RRC
tx_config.filterLength       = 400  ; % Cantidad de coeficientes del filtro
tx_config.filterRolloff      = 0.2  ; % Exceso de ancho de banda del filtro
tx_config.oversamplingFactor = 16   ; % Frecuencia  de sampling  del filtro

L = 1e3;

tx = transmitter(tx_config);
[tx_out, ak] = tx.transmit(L);

figure
scatterplot(ak);

figure
plot(tx_out)


