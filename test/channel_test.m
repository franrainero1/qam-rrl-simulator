%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : channel_test.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 7 Jul. 2021
% Description 	: digital communications channel class test
%---------------------------------------------------------------------%

L = 10e3;


% ------------ Transmisor -----------------------------------
tx_config.modulationType        = 1    ; % Tipo de transmisor = QAM
tx_config.simbolRate            = 32e9 ; % Baud Rate BR
tx_config.mapperLevels          = 2    ; % Cantidad de niveles del mapper M
tx_config.filterType            = 1    ; % Tipo de filtro conformador RRC
tx_config.filterLength          = 120  ; % Cantidad de coeficientes del filtro
tx_config.filterRolloff         = 0.2  ; % Exceso de ancho de banda del filtro
tx_config.oversamplingFactor    = 4    ; % Frecuencia  de sampling  del filtro

tx = transmitter (tx_config);
[tx_out, tx_ak] = tx.transmit(L);

% ------------ Canal ---------------------------------------
% Configuracion del canal
EbNo_dB = 10;

% --- Calculos para determinar var channel ---
EbNo            = 10^(EbNo_dB/10);
SNR_slicerInput = EbNo * log2(tx_config.mapperLevels);
SNR_channel     = SNR_slicerInput / tx_config.oversamplingFactor;
signalPower     = var(tx_out);
noisePower      = signalPower / SNR_channel;
ch_config.complexNoiseVariance  = noisePower ; % Varianza del ruido COMPLEJO introducido por ch 
% ---------------------------------------------------------

ch = channel     (ch_config);
ch_out = ch.addNoise(tx_out);

% --------------- Receptor ---------------------------------
rx_config = tx_config                  ; % Configuracion base del rx 
rx_config.correlatorPhase       = 0    ; % Fase de muestreo del correlador 
rx_config.dwConvPhaseError      = 0    ; % Error de fase en deg del down-converter 
rx_config.dwConvfrequencyOffset = 0    ; % Offset de frecuencia [Hz] del down converter


rx = receiver    (rx_config);
[rx_ak, rx_correlator_out] = rx.receive(ch_out);

close all

%hold all
%scatterplot(rx_ak);
figure
scatter(real(tx_ak), imag(tx_ak),'r*');
hold on
scatter(real(rx_ak), imag(rx_ak),'b.');
scatter(real(rx_correlator_out),imag(rx_correlator_out),'m.');
grid on

