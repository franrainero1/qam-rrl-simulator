clc; clear all; close all;

EbNo_dB = 0:0.1:16;
EbNo = 10.^(EbNo_dB/10);

M = [4 16];
sweeps = 10;
sweeped_EbNos = [1.5:1:10.5 ; 4.5:9.7/9:14.2];

figure

for i = 1:length(M)
    
    % Bit-error probability Pb
    Pb = 4/log2(M(i)) * ( 1-2^(-log2(M(i))/2)) * 0.5 * erfc( sqrt((3*log2(M(i))*EbNo)/(M(i)-1)) / sqrt(2) ); % Bit-error probability
    
    % Matlab BER calc function
    ber_MATLAB = berawgn(EbNo_dB, 'qam', M(i));
    
    % Simulation BER results
    simulated_BER = zeros(1, sweeps);
    format longG
    for j = 1:sweeps
        file_path = "../out/QAM_" + num2str(M(i)) + "_EbNodB_" + num2str(sweeped_EbNos(i, j), '%.2f') + "_RRC_rolloff_0.10/log/ber.txt";
        file = readtable(file_path);
        simulated_BER(j) = file.Var3(2);
    end
    
    % Plots
    if i == 1
        plot_colours = [[.5 0 .5];[1 .8 0]];
        marker = '+k';
    else
        plot_colours = [[1 0 0];[0 0 1]];
        marker = 'xk';
    end
    semilogy(EbNo_dB, ber_MATLAB, '-','LineWidth', 1.5, 'DisplayName', "berawgn() @ M=" + num2str(M(i)), 'Color', plot_colours(1,:))
    hold on
    semilogy(EbNo_dB, Pb, '--', 'LineWidth', 1.5, 'DisplayName', "$P_b$ @ M=" + num2str(M(i)), 'Color', plot_colours(2,:))
    scatter(sweeped_EbNos(i,:), simulated_BER, marker, 'LineWidth', 1.5, 'DisplayName', "Simul. BERs @ M=" + num2str(M(i)))

end

ylabel("Bit-Error Rate (BER)", 'Interpreter', 'latex')
xlabel("$E_b/N_0$ [dB]", 'Interpreter', 'latex')
legend('Interpreter', 'latex', 'Location', 'southwest')
ylim([6e-9 0.5])
xlim([0 17])
grid on
set(gcf, 'Position', [100 100 400 400],'Color', 'w')
saveas(gcf, 'BERvsEbN0plot.svg', 'svg')















